var server = require('http').Server();

var io = require('socket.io')(server);


// This will return a class equivalent
var Redis = require('ioredis');


//Creating object for the defined redis class
var redis = new Redis();

redis.on('connect', () => {
    console.log('Connected to Redis');
    // console.log('subscription_set:', redisClient.subscription_set);
});

redis.subscribe('laravel_database_barcode-channel');

redis.subscribe('laravel_database_weightlog-channel');



// redis.on('subscribe', (channel, count) => {
//     console.log('Subscribed to: ', channel, count);
// });

redis.on('message',function(channel, message){

    // console.log('Message received');
    //
    // console.log(message, channel);

    message = JSON.parse(message);

    console.log(message)

    io.emit(channel + ':' + message.event, message.data);
});


server.listen(3000, function(){
    console.log('server running');
});
