<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewindingProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rewinding_processes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('unique_id');
            $table->string('doff_no');
            $table->dateTime('doff_date');
            $table->string('material_id');
            $table->string('material');
            $table->string('floor_code');
            $table->tinyInteger('process_status')->default(1);
            $table->tinyInteger('erp_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rewinding_processes');
    }
}
