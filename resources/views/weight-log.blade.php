<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Weight Log</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />
    <style>
        #main{
            margin-top: 90px;
        }
        .table-div{
            max-height: 80vh;
            overflow-y: auto;
        }
        .select-css{
            width: 100%;
        }
        .select-50{
            width: 50%;
        }
        .text-input {
            width: 100%;
            height: 30px;
            border: 1px solid #888888;
            padding: 10px;
            border-radius: 5px;
        }
        .text-input-50 {
            width: 48%;
            height: 30px;
            border: 1px solid #888888;
            padding: 10px;
            border-radius: 5px;
        }
        select{
            height: 30px !important;
        }
        #scale,#actual{
          font-size: 25px;
          font-weight: 800;
          background-color: #91adff;
          color: #141b14;
        }
        #actual{
          font-size: 20px;
        }
        .weight_btn{
          display: inline-block;
          padding: 10px;
          width: 100%;
          margin-left: 1px;
          text-align: center;
          margin-top: 10px;
          color: white;
          font-weight: bold;
          font-size: 30px;
        }
        label[for='ok']{
          background-color: green;
        }
        label[for='not-ok']{
          background-color: red;
        }
        input[type="text"][disabled],select[disabled] {
           background-color: #99999947;
        }
        .delete-spl{
            text-align: center;
            color: #999999;
        }
        .delete-spl:hover{
            cursor: pointer;
            color: #91adff;
        }
        .reason-li{
          list-style: none;
          padding: 5px;
          font-weight: 600;
        }
        .reason-li:hover{
          background-color: #91adff;
        }
        .selected-reason{
          background-color: #004cb4;
          color: white;
        }
    </style>

</head>
<body>
    <section id="header">
        <header>
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar-collapse" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                    </div>
    
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><a class="active-menu" href="/">Home</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
    
    </section>
    <section id="main">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-md-6 col-lg-6">
                <form action="/create-weight-log" id="weight-log-form" method="POST" class="col-md-12 col-lg-12">
                    @csrf
                    <div class="col-md-6 col-lg-6 form-group">
                        <label for="doff">Doff No:</label>
                        <input type="hidden" name="doff_no" value="{{$rewindingProcess->doff_no}}">
                        <input type="hidden" name="doff_date" value="{{$rewindingProcess->doff_date}}">
                        <select name="doff" class="select-css" required>
                            @foreach ($doffs as $item)
                                @if ($item->id == $doff)
                                    <option value="{{$item->id}}" selected>{{$item->doff_no}}</option>                                    
                                @else
                                    {{-- <option value="{{$item->id}}">{{$item->doff_no}}</option> --}}
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6 col-lg-6 form-group">
                            <label for="spindle">Spindle:</label>
                            <input type="text" class="text-input" name="spindle" value="{{($weightLog->last()->spindle ?? 0) + 1}}" required>
                        </div>
                    <div class="col-md-12 col-lg-12 form-group">
                        <input type="hidden" name="material_id" value="{{$rewindingProcess->material_id}}">
                        <input type="hidden" name="material" value="{{$rewindingProcess->material}}">
                        <input type="hidden" name="floor_code" value="{{$rewindingProcess->floor_code}}">
                        <label for="new_material">Material:</label> 
                        <select name="new_material" class="select-css" required>
                            @foreach ($itemMaster as $item)
                                @if (!empty($weightLog->last()->new_material))
                                    @if ($weightLog->last()->new_material == $item->packing_name)
                                        <option value="{{$item->packing_name}}" selected>{{$item->packing_name}}</option>
                                    @else
                                        <option value="{{$item->packing_name}}">{{$item->packing_name}}</option>                                    
                                    @endif
                                @else
                                    <option value="{{$item->packing_name}}">{{$item->packing_name}}</option>                                                                        
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-12 col-lg-12 p-lr-0">
                        <div class="col-md-6 col-lg-6 form-group">
                            <label for="tare">Tare Weight:</label>
                            <select id="tare" class="select-50">
                                <option value="0">Manual</option>
                                @foreach ($bobbins as $bobbin)
                                    @if (!empty($weightLog->last()->tare_weight))
                                        @if ($weightLog->last()->tare_weight == $bobbin->tare_weight)
                                            <option value="{{$bobbin->tare_weight}}" selected>{{$bobbin->name}}</option>
                                        @else
                                            <option value="{{$bobbin->tare_weight}}">{{$bobbin->name}}</option>
                                        @endif
                                    @else
                                        <option value="{{$bobbin->tare_weight}}">{{$bobbin->name}}</option>                                        
                                    @endif
                                @endforeach
                            </select>
                            <input type="text" class="text-input-50" id="tare-weight" name="tare_weight" value="{{$weightLog->last()->tare_weight ?? '0'}}" required>
                        </div>
                        <div class="col-md-6 col-lg-6 form-group">
                            <label for="op-name">Operator:</label>
                            <input type="text" id="op-name" class="text-input" name="op_name" value="{{$weightLog->last()->op_name ?? ''}}" required>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 form-group">
                            <label for="scale">Scale Weight:</label>
                            <input type="text" id="scale" class="text-input" name="total_weight" required>
                    </div>
                    <div class="col-md-6 col-lg-6 form-group">
                            <label for="actual">Actual Weight:</label>
                            <input type="text" id="actual" class="text-input" name="material_weight" required>
                    </div>
                    <div class="col-md-6 form-group">
                        <input type="hidden" name="weight_status" value="">
                        <label for="ok" class="weight_btn">OK</label>
                        <input type="checkbox" class="weight_status" value="1" id="ok">
                    </div>
                    <div class="col-md-6 form-group">
                        <input type="hidden" name="reason" value="">
                        <input type="hidden" name="ncr_status" value="0">
                        <label for="not-ok" class="weight_btn">Not OK</label>
                        <input type="checkbox" class="weight_status" value="0" id="not-ok">
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ok-weight">Ok Weight</label>
                            <input type="text" class="text-input" value="{{$total['ok']}}" id="ok-weight" disabled>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="not-ok-weight">Not Ok Weight</label>
                            <input type="text" class="text-input" value="{{$total['not_ok']}}" id="not-ok-weight" disabled>
                        </div>
                    </div>
    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="total-weight">Total Weight</label>
                            <input type="text" class="text-input" value="{{$total['wt']}}" id="total-weight" disabled>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="total-entries">Total Entries</label>
                            <input type="text" class="text-input" value="{{$total['entry']}}" id="total-entries" disabled>
                        </div>
                    </div>
                </form>
            </div>


            <div class="col-md-3 col-lg-3 table-div">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Spindle</th>
                            <th>Weight</th>
                            <th>Status</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody> 
                        @foreach ($weightLog as $key=>$item)
                            @if ($key <= $split)
                                <tr>
                                    <td>{{$item->spindle}}</td>
                                    <td>{{$item->material_weight}}</td>
                                    @if ($item->weight_status == 1)
                                        <td>Ok</td>
                                    @else
                                        <td>Not Ok</td>                                    
                                    @endif
                                    <td class="delete-spl" data-id="{{$item->id}}"><i class="material-icons">delete</i></td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>


            <div class="col-md-3 col-lg-3 table-div">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Spindle</th>
                            <th>Weight</th>
                            <th>Status</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody> 
                        @foreach ($weightLog as $key=>$item)
                                @if ($key > $split)
                                <tr>
                                    <td>{{$item->spindle}}</td>
                                    <td>{{$item->material_weight}}</td>
                                    @if ($item->weight_status == 1)
                                        <td>Ok</td>
                                    @else
                                        <td>Not Ok</td>                                    
                                    @endif
                                    <td class="delete-spl" data-id="{{$item->id}}"><i class="material-icons">delete</i></td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <div id="reason" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Select Reason for Not Ok</h4>
                </div>
                <div class="modal-body">
                    <ul>
                    <?php foreach ($reason as $key => $value): ?>
                        <?php if ($value->ncr_account == "NCR (D)"): ?>
                        <li class="reason-li" data-ncr="1">{{$value->defect}}</li>
                        <?php else: ?>
                            <li class="reason-li" data-ncr="0">{{$value->defect}}</li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="not-ok-save">Save</button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var socket = io('http://127.0.0.1:3000');


            socket.on('laravel_database_weightlog-channel:weightLogEvent', function(data){
                console.log('Weightlog Data : '+data);
                $('#scale').val(data);
                var tare = $('#tare-weight').val();
                $('#actual').val((parseFloat(data)-parseFloat(tare)).toFixed(3));
                // showBarcodeData(data);
            });



            $('.weight_status').on('click',function() {
                if ($(this).val() == 1) {
                    $('input[name="reason"]').val("");
                    $('input[name="ncr_status"]').val("0");
                    $('input[name="weight_status"]').val($(this).val());
                    $('#weight-log-form').submit();
                }else{
                    $('input[name="weight_status"]').val($(this).val());
                    $('#reason').modal('show');
                }
            });
            $('select[name="doff"]').on('change',function(){
                window.location.href = "/weight-log/"+$(this).val();
            });
            $('#tare').on('change',function() {
                $('#tare-weight').val($(this).val());
                $('#actual').val(parseFloat(($('#scale').val())-parseFloat($(this).val())).toFixed(2));
            });
            $('#scale').on('input propertychange',function(){
                var tare = $('#tare-weight').val();
                $('#actual').val((parseFloat($(this).val())-parseFloat(tare)).toFixed(2));
            });
            $('#tare-weight').on('input',function() {
                $('#actual').val(parseFloat(($('#scale').val())-parseFloat($(this).val())).toFixed(2));
            });
            $('.weight_status').on('click',function() {
                console.log($(this).val());
                
                if ($(this).val() == 1) {
                    $('input[name="reason"]').val("");
                    $('input[name="ncr_status"]').val("0");
                    $('input[name="weight_status"]').val($(this).val());
                    $('#weight-log-form').submit();
                }else{
                    $('input[name="weight_status"]').val($(this).val());
                    $('#reason').modal('show');
                }
            });

            $('.reason-li').on('click',function() {
                $('input[name="reason"]').val($(this).text());
                $('input[name="ncr_status"]').val($(this).data('ncr'));
                $('.selected-reason').removeClass('selected-reason');
                $(this).addClass('selected-reason');
            });

            $('#not-ok-save').on('click',function() {
                $('#weight-log-form').submit();
            });

            $('.delete-spl').on('click',function(){
                var formData = 'wl_id='+$(this).data('id');
                $.ajax({
                    type:'POST',
                    url:'/delete-spindle',
                    data:formData,
                    success:function(data){
                        if (data.status == '1') {
                            location.reload();
                        }else if (data.status == '0') {
                            alert('Unable to update ERP - '+data.msg);
                            location.reload();
                        }else{
                            alert(data.msg);
                        }
                    },
                    error:function(xhr){
                        console.log(xhr.status);
                    }
                });
            });

            // $('#weight-log-form').on('submit',function(e){
            //     e.preventDefault();
            //     if (parseFloat($('#scale').val()) > parseFloat($('#tare').val())) {
            //         var formData = $(this).serialize();
            //         $.ajax({
            //             type:'POST',
            //             url:'/create-weight-log',
            //             data:formData,
            //             success:function(data){
            //                 if (data.status == '1') {
            //                     location.reload();
            //                 }else if (data.status == '0') {
            //                     alert('Unable to update ERP - '+data.msg);
            //                     location.reload();
            //                 }else{
            //                     alert(data.msg);
            //                 }
            //             },
            //             error:function(xhr){
            //                 console.log(xhr.status);
            //             }
            //         });
            //     }else{
            //         alert('Scale Weight should be higher then Tare weight Try new Weight.');
            //     }
            // });

        });
    </script>
</body>
</html>