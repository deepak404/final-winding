<!DOCTYPE html>
<html>
<head>
    <title>UDFW Weight Log</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link rel="stylesheet" href="{{url('css/generate-indent.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />

    <style>

        h3{
            margin-top: 0px;
        }
        .material-icons{
            vertical-align: middle;
        }


        .material-icons:hover{
            cursor: pointer;
        }


        select{
            height: 30px;
        }


        label{
            margin-bottom: 10px;
        }


        select, input{
            width: 100%;
            height: 30px !important;
        }

        .form-group{
            margin-bottom: 10px;
        }

        input[type="submit"]{
            margin-top: 30px;
        }

        #recorded-weight-log,#recorded-weight-log-E{
            height: 80vh;
            margin-top: 50px;
            overflow: scroll;
            /* overflow-x:hidden; */
        }
        #main{
            margin-top: 40px;
        }
        input[type="text"][disabled],select[disabled] {
           background-color: #99999947;
        }
        #scale-weight,#actual-weight{
          font-size: 25px;
          font-weight: 800;
          background-color: #91adff;
          color: #141b14;
        }
        #actual-weight{
          font-size: 20px;
        }
        #overwrite>.modal-dialog{
          width: 25%;
        }
        .update-btn{
          width: 49%;
          margin: 20px 0px;
        }
        label[for="material"]{
          display: inline-block;
        }
        #recorded-weight-log,#recorded-weight-log-E{
          padding: 0px !important;
        }
        #recorded-weight-log>table,#recorded-weight-log-E>table{
          font-size: 10px;
          font-weight: bold;
          text-align: center;
        }
        #recorded-weight-log>table>tbody>tr>td,#recorded-weight-log-E>table>tbody>tr>td{
          padding: 1px;
          vertical-align: middle;
        }
        #recorded-weight-log>table>tbody>tr>td:first-child:hover,#recorded-weight-log-E>table>tbody>tr>td:first-child:hover{
          background-color: #91adff;
          cursor:pointer;
        }
        .p-rl-0{
          padding-left: 0px !important;
          padding-right: 5px  !important;
        }
        .weight_btn{
          display: inline-block;
          padding: 10px;
          width: 100%;
          margin-left: 1px;
          text-align: center;
          margin-top: 10px;
          color: white;
          font-weight: bold;
          font-size: 30px;
        }
        label[for='ok']{
          background-color: green;
        }
        label[for='not-ok']{
          background-color: red;
        }
        ul{
          padding: 0px;
          max-height: 200px;
          overflow: auto;
        }
        .reason-li{
          list-style: none;
          padding: 5px;
          font-weight: 600;
        }
        .reason-li:hover{
          background-color: #91adff;
        }
        .selected-reason{
          background-color: #004cb4;
          color: white;
        }
        #weight-log-report>input{
          width: 49% !important;
        }
        .material-icons{
          font-size: 15px !important;
        }
        .selected-td{
          background-color: #004cb4;
          color: white;
        }
    </style>
</head>
<body>
    <div id="loader" class="loader"></div>
<section id="header">
    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a class="active-menu" href="/">Home</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

</section>


<section id="main">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">

                <div class="col-md-6 col-lg-6 col-sm-6">
                    <form action="#" class="col-md-12" name="weight-log-form" id="weight-log-form">
                      <input type="hidden" name="machine" value="UDFW">
                      <input type="hidden" name="rw_status" value="1">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="unique_id">Unique ID:</label>
                          <input type="text" class="text-input" name="unique_id" id="unique_id" value="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="doff-no">DOFF No</label>
                          <input id="doff-input" type="text" class="text-input" value="{{$doff}}">
                          <input type="hidden" class="text-input" name="doff_no" id="doff-no" value="{{$doff}}">

                        </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="material">Material</label>
                              <select name="material" id="material">
                                <?php if (is_null($material)): ?>
                                  <?php foreach ($itemMaster as $key => $value): ?>
                                    <?php if (!empty($lastDoff->material)): ?>
                                        <?php if ($lastDoff->material == $value->material): ?>
                                          <option value="{{$value->id}}" selected>{{$value->material}}</option>
                                        <?php else: ?>
                                          <option value="{{$value->id}}" >{{$value->material}}</option>
                                        <?php endif; ?>
                                      <?php else: ?>
                                        <option value="{{$value->id}}" >{{$value->material}}</option>
                                    <?php endif; ?>
                                  <?php endforeach; ?>
                                  <?php else: ?>
                                    <option value="{{$material['id']}}">{{$material['material']}}</option>
                                <?php endif; ?>
                              </select><span id="floor_code">{{$last->floor_code ?? $itemMaster[0]->descriptive_name }}</span>
                          </div>
                      </div>


                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="spindle-no">Spindle No</label>
                                  <input type="text" class="text-input" name="spindle_no" id="spindle-no" value="" required>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                          <div class="form-group">
                              <label for="filament">Filament</label>
                              <select name="filament" id="filament">
                                <?php if (!empty($last->filament_type)): ?>
                                  <?php foreach ($filament as $key => $value): ?>
                                    <?php if ($last->filament_type == $value->name): ?>
                                      <option value="{{$value->name}}" selected>{{$value->name}}</option>
                                      <?php else: ?>
                                      <option value="{{$value->name}}">{{$value->name}}</option>
                                    <?php endif; ?>
                                  <?php endforeach; ?>
                                  <?php else: ?>
                                    <?php foreach ($filament as $key => $value): ?>
                                      <?php if (!empty($lastDoff->filament_type)): ?>
                                          <?php if ($lastDoff->filament_type == $value->name): ?>
                                            <option value="{{$value->name}}" selected>{{$value->name}}</option>
                                          <?php else: ?>
                                            <option value="{{$value->name}}">{{$value->name}}</option>
                                          <?php endif; ?>
                                      <?php else: ?>
                                          <option value="{{$value->name}}">{{$value->name}}</option>
                                      <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                              </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tare-weight">Tare Weight</label>
                                <select class="text-input" id='tare-option' style="width:49% !important;">
                                    <option value="">Manual</option>
                                    <?php foreach ($bobbins as $key => $value): ?>
                                        <?php if (!empty($last->tare_weight)): ?>
                                            <?php if ($value->tare_weight == $last->tare_weight): ?>
                                              <option value="{{$value->tare_weight}}" selected>{{$value->name}}</option>
                                                <?php else: ?>
                                              <option value="{{$value->tare_weight}}">{{$value->name}}</option>
                                            <?php endif ?>
                                            <?php else: ?>
                                              <option value="{{$value->tare_weight}}">{{$value->name}}</option>
                                        <?php endif ?>
                                    <?php endforeach; ?>
                                </select>
                                <input type="text" class="text-input" name="tare_weight" id="tare-weight" value="{{$last->tare_weight ?? 0}}" style="width:49% !important;" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="operator">Operator</label>
                                <input type="text" class="text-input" name="operator" id="operator" value="{{$operator}}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="doff-time">DOFF Time</label>
                                <?php if (is_null($doffDate)): ?>
                                  <input type="text" class="text-input date" name="doff_time" id="doff-time" value="{{$doffDate}}" >
                                  <?php else: ?>
                                    <input type="text" class="text-input" value="{{$doffDate}}" disabled>
                                    <input type="hidden" class="text-input" name="doff_time" id="doff-time" value="{{$doffDate}}">
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="scale-weight">Scale Weight</label>
                                <input type="text" class="text-input" name="scale_weight" id="scale-weight" required>


                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="actual-weight">Actual Weight</label>
                                <input type="text" class="text-input" name="actual_weight" id="actual-weight" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="hidden" name="weight_status" value="">
                                <label for="ok" class="weight_btn">OK</label>
                                <input type="checkbox" class="weight_status" value="1" id="ok">
                                <input type="hidden" name="ncr" value="0">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ok-weight">Ok Weight</label>
                                <input type="text" class="text-input" value="{{$weightLog->where('weight_status','1')->sum('total_weight')}}" id="ok-weight" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="not-ok-weight">Not Ok Weight</label>
                                <input type="text" class="text-input" value="{{$weightLog->where('weight_status','0')->sum('total_weight')}}" id="not-ok-weight" disabled>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="total-weight">Total Weight</label>
                                <input type="text" class="text-input" value="{{$weightLog->sum('total_weight')}}" id="total-weight" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="total-entries">Total Entries</label>
                                <input type="text" class="text-input" value="{{count($weightLog)}}" id="total-entries" disabled>
                            </div>
                        </div>
                        {{-- <div class="col-md-12 col-lg-12">
                          <button type="button" class="btn btn-primary pull-right" id="get-report">Report</button>
                        </div> --}}
                    </form>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <div class="col-md-12" id="recorded-weight-log-E">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Spindle</th>
                                <th>Weight(kg)</th>
                                <th>Status</th>
                                <th>Delete</th>
                                <th>Print QR</th>
                            </tr>
                            </thead>
                            <tbody>
                              @foreach ($weightLog as $key=>$item)
                                @if ($key < $split)
                                    <tr>
                                        <td>{{$item->spindle}}</td>
                                        <td>{{$item->material_weight}}</td>
                                        @if ($item->weight_status == 1)
                                            <td>Ok</td>
                                        @else
                                            <td>Not Ok</td>                                    
                                        @endif
                                        <td class="delete-spl" data-id="{{$item->id}}"><i class="material-icons">delete</i></td>
                                        <td style="text-align:center;"><i class="material-icons print-qr" data-id="{{$item->id}}">print</i></td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <div class="col-md-12" id="recorded-weight-log">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Spindle</th>
                                <th>Weight(kg)</th>
                                <th>Status</th>
                                <th>Delete</th>
                                <th>Print QR</th>
                            </tr>
                            </thead>
                            <tbody>
                              @foreach ($weightLog as $key=>$item)
                                @if ($key >= $split)
                                    <tr>
                                        <td>{{$item->spindle}}</td>
                                        <td>{{$item->material_weight}}</td>
                                        @if ($item->weight_status == 1)
                                            <td>Ok</td>
                                        @else
                                            <td>Not Ok</td>                                    
                                        @endif
                                        <td class="delete-spl" data-id="{{$item->id}}"><i class="material-icons">delete</i></td>
                                        <td style="text-align:center;"><i class="material-icons print-qr" data-id="{{$item->id}}">print</i></td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<div id="passModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3>Enter Password to Delete.</h3>
        <form id="delete-form" action="#">
          <input type="hidden" name="id" id="bobbin-id" value="">
          <input type="password" class="text-input" name="password" value="" required placeholder="Password">
          <input class="btn btn-primary" type="submit" name="open" value="Delete">
        </form>
      </div>
    </div>
  </div>
</div>


<div id="reason" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select Reason for Not Ok</h4>
      </div>
      <div class="modal-body">
        <ul>
          <?php foreach ($reason as $key => $value): ?>
            <?php if ($value->ncr_account == "NCR (D)"): ?>
                <li class="reason-li" data-ncr="1">{{$value->defect}}</li>
            <?php endif; ?>
          <?php endforeach; ?>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="not-ok-save">Save</button>
      </div>
    </div>
  </div>
</div>

<div id="weightLogReport" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3>Weight Log Report</h3>
        <form action="/weight-log-report" method="post" id="weight-log-report" target="_blank">
          @csrf
          <input class="text-input date" type="text" name="from" value="{{$fromDate}}" required placeholder="From Date">
          <input class="text-input date" type="text" name="to" value="{{date('d-m-Y H:i', strtotime(date('d-m-Y H:i')))}}" required placeholder="To Date">
          <input class="btn btn-primary" type="submit" value="Show Report">
        </form>
      </div>
    </div>
  </div>
</div>

<div id="doffModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3>Enter Doff No.</h3>
        <form class="" action="#">
          <input type="text" name="doff" value="" id="doff-val" required placeholder="New Doff">
          <input class="btn btn-primary" type="submit" name="open" value="Open Doff">
        </form>
      </div>
    </div>
  </div>
</div>
{{-- 
<div id="qrModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3>Scan QR Code.</h3>
        <form id="qrForm" method="post" action="">
          <input type="text" name="unique_id" value="" id="qr-val" required placeholder="Uniq ID">
          <input class="btn btn-primary" type="submit" name="open" value="Open Doff">
        </form>
      </div>
    </div>
  </div>
</div> --}}

<script src="{{url('/js/jquery-ui-1.12.1/external/jquery/jquery.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
<script type="text/javascript" src="{{url('js/loader.js')}}"></script>
<script src="{{url('js/jquery-ui-1.12.1/jquery-ui.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>


<script type="text/javascript">
$(document).ready(function(){
  $('.date').datetimepicker({
      format:'d-m-Y H:i',
  });
  var okWeight = 0;
  var notOkWeight = 0;
    var doff = "{{ $doff }}";
    var itemMaster = {!! json_encode($itemMaster->toArray()) !!};;
    // console.log(filament);

    $('input[type="text"],input[type="password"]').attr('autocomplete', 'off');

    $('#operator, #doff-val').on('input',function() {
      $(this).val($(this).val().toUpperCase());
    });

    $(document).on('input','#spindle-no' ,function() {
      $(this).val($(this).val().replace(/[^0-9]/gi, ''));
    });

    $('#scale-weight').on('input propertychange',function(){
      var tare = $('#tare-weight').val();
      $('#actual-weight').val(parseFloat(($(this).val())-parseFloat(tare)).toFixed(2));
    });

    $('#tare-weight').on('input',function() {
      $('#actual-weight').val(parseFloat(($('#scale-weight').val())-parseFloat($(this).val())).toFixed(2));
    });

    $('#tare-option').on('change',function() {
      $('#tare-weight').val($(this).val());
      $('#actual-weight').val(parseFloat(($('#scale-weight').val())-parseFloat($(this).val())).toFixed(2));
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var socket = io('http://127.0.0.1:3000');

    socket.on('weightlog-channel:WeightReceived', function(data){
       $('#scale-weight').val(data);
       $('#actual-weight').val((parseFloat(data)-parseFloat($('#tare-weight').val())).toFixed(2));
    });

    $('#status-weight').on('change',function() {
      // console.log($(this).val());
      if ($(this).val() == 0) {
        $('#status-reason').removeAttr('disabled');
      }else {
        $('#status-reason').attr('disabled','disabled');
      }

    });

    $('#weight-log-form').on('submit',function(e){
        e.preventDefault();
        // okWeight = (parseFloat(okWeight)+parseFloat($('#actual-weight').val())).toFixed(1);
          if (parseFloat($('#scale-weight').val()) > parseFloat($('#tare-weight').val())) {
            var formData = $(this).serialize();
            $.ajax({
              type:'POST',
              url:'/udfw-weight-create',
              data:formData,
              success:function(data){
                if (data.status == '1') {
                  location.reload();
                }else if (data.status == '0') {
                  alert('Unable to update ERP - '+data.msg);
                  location.reload();
                }else{
                  alert(data.msg);
                }
              },
              error:function(xhr){
                console.log(xhr.status);
              }
            });
          }else{
            alert('Scale Weight should be higher then Tare weight Try new Weight.');
          }
      });

      $('.delete-spl').on('click',function() {
        $('#bobbin-id').val($(this).data('id'));
        $('#passModal').modal('show');
      });

      $('#delete-form').on('submit',function(e) {
        e.preventDefault();
        var formData = $(this).serialize();
        $.ajax({
                type:'POST',
                url:'/delete-weight-log',
                data:formData,
                success:function(data){
                    if (data.status == '1') {
                      location.reload();
                    }else{
                        alert(data.msg);
                    }
                },
                error:function(xhr){
                    console.log(xhr.status);
                }
            });
      });

      $('.print-qr').on('click',function() {
        $.ajax({
                type:'POST',
                url:'/print-qr',
                data:'id='+$(this).data('id'),
                success:function(data){
                  // location.reload();
                },
                error:function(xhr){
                    console.log(xhr.status);
                }
            });
      });

      $('#material').on('change',function() {
        var selectedId = $(this).val();
        $.each(itemMaster, function(index, val) {
          if (val.id == selectedId) {
            $('#floor_code').text(val.descriptive_name);
          }
        });
      });

      $('.weight_status').on('click',function() {
        if ($(this).val() == 1) {
          $('input[name="reason"]').val("");
          $('input[name="ncr"]').val("0");
          $('input[name="weight_status"]').val($(this).val());
          $('#weight-log-form').submit();
        }else{
          $('input[name="weight_status"]').val($(this).val());
          $('#reason').modal('show');
        }
      });

      $('#doff-input').on('click',function() {
        $('#doffModal').modal('show');
      });

      $('#doffModal').on('submit',function(e) {
        e.preventDefault();
        window.location.href = "/rewinding/"+$('#doff-val').val();
      });
      
      //setup before functions
      var typingTimer;                //timer identifier
      var doneTypingInterval = 1000;  //time in ms, 1 second for example


      $('input[name="unique_id"]').on('input', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
      });

      function doneTyping () {
        if ($('input[name="unique_id"]').val().length > 0) {
          $.ajax({
                type:'post',
                url:'/getQR',
                data:'unique_id='+$('input[name="unique_id"]').val(),
                success:function(data){
                  if (data.status) {
                    var entry = data.entry;
                      $.each(entry,function (key,value) {
                        if (value.weight_status == 0) {
                          notOkWeight = parseFloat(notOkWeight)+parseFloat(value.material_weight);
                          $.each(itemMaster, function(index, val) {
                            if (val.material == value.material) {
                              $('#material').val(val.id);
                            }
                          });
                          $('#floor_code').text(value.floor_code);
                          $('#tare-option').val(value.tare_weight);
                          $('#tare-weight').val(value.tare_weight);
                          $('#unique_id').val(value.unique_id);
                          $('#filament').val(value.filament);
                          $('#spindle-no').focus();
                        }else{
                          okWeight = parseFloat(okWeight)+parseFloat(value.material_weight);
                        }
                      });
                  }else{
                    alert(data.msg);
                    location.reload();
                  }

                },
                error:function(xhr){
                    console.log(xhr.status);
                }
              });
        }else{
          console.log("No Scanded data.");
        }
      }

});
</script>
</body>
</html>
