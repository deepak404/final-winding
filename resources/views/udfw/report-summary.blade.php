<!DOCTYPE html>
<html>
<head>
    <title>UDFW Weight Log</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />

    <style media="screen">
      #main{
        margin-top: 70px;
      }
      input[type='checkbox']{
        display: block !important;
        width: 20%;
      }
      .table-div{
        margin-top: 30px;
        height: 67vh;
        overflow: auto;
      }
      input[type='text']{
        height: 30px !important;
        width: 100%;
      }
      input[disabled]{
        background-color: #d3d3d3;
      }
    </style>

</head>
<body>
    <div id="loader" class="loader"></div>
<section id="header">
    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a class="active-menu" href="/">Home</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

</section>


<section id="main">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
              <div class="col-md-8 col-lg-8 col-sm-12">
                <form class="summary-form col-md-12 col-lg-12 col-sm-12"  style="padding:5px 0px" action="#">
                  <div class="co-md-12 col-lg-12 col-sm-12" style="padding:5px 0px">
                      <div class="form-group col-md-2">
                        <label for="weight-check">Weight</label>
                        <input type="checkbox" class="text-input" name="type" id="weight-check" value="weight">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="from-weight">Weight Form</label>
                        <input type="text" class="text-input date weight" name="from" id="from-weight" value="{{$fromDate}}" required disabled>
                      </div>
                      <div class="form-group col-md-4">
                        <label for="to-weight">Weight To</label>
                        <input type="text" class="text-input date weight" name="to" id="to-weight" value="{{date('d-m-Y H:i')}}" required disabled>
                      </div>
                  </div>
                  <div class="co-md-12 col-lg-12 col-sm-12" style="padding:5px 0px">
                      <div class="form-group col-md-2">
                        <label for="doff-check">Doff</label>
                        <input type="checkbox" class="text-input" name="type" id="doff-check" value="doff">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="from-doff">Doff Form</label>
                        <input type="text" class="text-input date doff" name="from" id="from-doff" value="{{$fromDate}}" required disabled>
                      </div>
                      <div class="form-group col-md-4">
                        <label for="to-doff">Doff To</label>
                        <input type="text" class="text-input date doff" name="to" id="to-doff" value="{{date('d-m-Y H:i')}}" required disabled>
                      </div>
                  </div>
                  <div class="co-md-12 col-lg-12 col-sm-12">
                    <input type="submit" class="btn btn-primary center-block" value="Get Summary">
                  </div>
                </form>
              </div>
              <div class="col-md-4 col-lg-4 col-sm-1">
                <div class="col-md-12 col-lg-12 col-sm-12 table-div">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Count</th>
                          <th>Doff</th>
                        </tr>
                      </thead>
                      <tbody id="summary-tbody">

                      </tbody>
                    </table>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12">
                  <form class="col-md-12 col-lg-12 col-sm-12" action="/generate-report" method="post" target="_blank">
                    <div class="col-md-12 col-lg-12">
                      @csrf
                      <input type="hidden" name="report_type" id="report_type" value="">
                      <input type="hidden" name="from" id="report_from" value="">
                      <input type="hidden" name="to" id="report_to" value="">
                      <input type="submit" class="btn btn-primary center-block" value="Generate Report">
                    </div>
                  </form>
                </div>
              </div>
            </div>
        </div>
    </div>
</section>


<script src="{{url('/js/jquery-ui-1.12.1/external/jquery/jquery.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
<script type="text/javascript" src="{{url('js/loader.js')}}"></script>
<script src="{{url('js/jquery-ui-1.12.1/jquery-ui.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
      $('.date').datetimepicker({
          format:'d-m-Y H:i',
      });
      $('input[type="text"],input[type="password"]').attr('autocomplete', 'off');
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $('input[type="checkbox"]').on('click',function() {
        if($(this).val() == 'doff'){
          if($(this). prop("checked") == true){
            $(this). prop("checked",true);
            $('input[name="weight"]'). prop("checked",false);
            $('.doff').removeAttr('disabled');
            $('.weight').attr('disabled','disabled');
          }else{
            $(this). prop("checked",false);
            $('input[name="weight"]'). prop("checked",true);
            $('.doff').attr('disabled','disabled');
          }
        }else{
          if($(this). prop("checked") == true){
            $(this). prop("checked",true);
            $('input[name="doff"]'). prop("checked",false);
            $('.weight').removeAttr('disabled');
            $('.doff').attr('disabled','disabled');
          }else{
            $(this). prop("checked",false);
            $('input[name="doff"]'). prop("checked",true);
            $('.weight').attr('disabled','disabled');
          }
        }
      });
      $('.summary-form').on('submit',function(e) {
        e.preventDefault();
        var formArray = $(this).serializeArray();
        var formData = $(this).serialize();
        $('#report_type').val(formArray[0].value);
        $('#report_from').val(formArray[1].value);
        $('#report_to').val(formArray[2].value);
        $.ajax({
          type:'POST',
          url:'/get-summary',
          data:formData,
          success:function(data){
            if (data.status) {
              $('#summary-tbody').empty();
              $.each(data.logs, function(key, value) {
                $('#summary-tbody').append('<tr><td>'+value.count+'</td><td>'+value.doff+'</td></tr>');
              });
            }
          },
          error:function(xhr){
            console.log(xhr.status);
          }
        });
      });
    });
</script>
</body>
</html>
