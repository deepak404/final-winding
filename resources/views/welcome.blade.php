<!DOCTYPE html>
<html>
<head>
    <title>Weight Log</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />

    <style media="screen">
      #doff-val, #udfw-doff-val{
        width: 49%;
        height: 42px;
        margin-bottom: 10px;
        border: 1px solid #00000066;
        border-radius: 5px;
        padding: 12px;
      }
      #doff-pass, #udfw-doff-pass{
        width: 100%;
        height: 42px;
        margin-bottom: 10px;
        border: 1px solid #00000066;
        border-radius: 5px;
        padding: 12px;
      }
      #old-doff{
        width: 50%;
      }
      .material-icons{
           color:#4183D7 !important;
           font-size: 55px !important;
      }

      #weight-log-form>.text-input{
        width: 49%;
        margin: 10px 0px;
      }
      #weight-log-form>input[type='submit']{
        margin: 10px 0px;
      }
      .box-part:hover{
        cursor: pointer;
      }
      #doff,#rewinding-pass,#qr-pass,#doff_no, #udfw-doff-rw{
        width: 100%;
        margin: 10px 0px;
        padding: 10px;
        border-radius: 5px;
        border: 1px solid black;
        text-align: center;
      }
      .title {
          margin-top: 15px;
      }
      #main{
        margin-top: 75px;
      }
    </style>
</head>
<body>
<section id="header">
    <header>

        <div class="container">
        </div>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                </div>
            </div>
        </nav>
    </header>

</section>


<section id="main">
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div id="rewinding-confirmation" class="box-part text-center">
              <i class="material-icons">find_in_page</i>
              <div class="title">
                <a>Rewinding Confirmation</a>
              </div>
             </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div id="weight-log" class="box-part text-center">
              <i class="material-icons">layers</i>
              <div class="title">
                <a>Weight Log</a>
              </div>
           </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div id="weight-log-report" class="box-part text-center">
              <i class="material-icons">library_books</i>
              <div class="title">
                  <a>Report</a>
              </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div id="" class="box-part text-center">
              <i class="material-icons">cancel_presentation</i>
              <div class="title">
                  <a>Doff Close</a>
              </div>
              </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div id="lf-rm" class="box-part text-center">
              <i class="material-icons">find_replace</i>
              <div class="title">
                  <a>Remanent / Leftover</a>
              </div>
              </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div id="send-erp" class="box-part text-center">
              <i class="material-icons">backup</i>
              <div class="title">
                  <a>Send to ERP - ({{$count}})</a>
              </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div id="udfw-weightlog" class="box-part text-center">
              <i class="material-icons">format_color_reset</i>
              <div class="title">
                  <a>Undipped Weight Log</a>
              </div>
              </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div id="udfw-rewinding" class="box-part text-center">
              <i class="material-icons">360</i>
              <div class="title">
                  <a>Undipped Rewinding</a>
              </div>
              </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div id="udfw-ncr" class="box-part text-center">
              <i class="material-icons">leak_remove</i>
              <div class="title">
                  <a>Undipped NCR</a>
              </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div id="" class="box-part text-center">
              <i class="material-icons">library_books</i>
              <div class="title">
                  <a>Undipped WeightLog Report</a>
              </div>
              </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div id="reportSummary" class="box-part text-center">
              <i class="material-icons">library_books</i>
              <div class="title">
                  <a>Undipped WeightLog Summary</a>
              </div>
              </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div id="udfw-send-erp" class="box-part text-center">
            <i class="material-icons">backup</i>
            <div class="title">
                <a>Send to ERP - ({{$udfwCount}})</a>
            </div>
            </div>
        </div>
        </div>
    </div>
</section>


<div id="doffModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3>Enter Doff No.</h3>
        <form class="" action="#">
          <select id='doff' name="doff">
            <option value="" selected disabled>Doff No</option>
            @foreach ($rewindingProcess as $item)
                <option value="{{$item->id}}">{{$item->doff_no}}</option>
            @endforeach 

          </select>
          <input type="password" id="doff-pass" required placeholder="Password">
          <input class="btn btn-primary" type="submit" name="open" value="Open Doff">
        </form>
      </div>
    </div>
  </div>
</div>

<div id="udfwDoffModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3>Enter Doff No.</h3>
        <form class="" action="#">
          <input type="text" name="doff" value="" id="udfw-doff-val" required placeholder="New Doff">
          <select id='old-doff' name="old-dof">
            <option value="" selected disabled>Old Doff</option>
            <?php foreach ($doffs as $key => $value): ?>
              <option value="{{$value}}">{{$value}}</option>
            <?php endforeach; ?>
          </select>
          <input type="password" id="udfw-doff-pass" required placeholder="Password">
          <input class="btn btn-primary" type="submit" name="open" value="Open Doff">
        </form>
      </div>
    </div>
  </div>
</div>


<div id="weightLogReport" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3>Weight Log Report</h3>
        <form action="/weight-log-report" method="post" id="weight-log-form" target="_blank">
          @csrf
          <input class="text-input date" type="text" name="from" value="{{$fromDate}}" required placeholder="From Date">
          <input class="text-input date" type="text" name="to" value="{{date('d-m-Y H:i')}}" required placeholder="To Date">
          <input class="btn btn-primary" type="submit" value="Show Report">
        </form>
      </div>
    </div>
  </div>
</div>


<div id="rewindingDoffModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3>Enter Doff No.</h3>
        <form class="" action="#">
          <input type="text" name="doff" value="" id="udfw-doff-rw" required placeholder="New/Old Doff">
          <input type="password" id="rewinding-pass" placeholder="Password">
          <input class="btn btn-primary" type="submit" name="open" value="Open Doff">
        </form>
      </div>
    </div>
  </div>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>
<script type="text/javascript">
    $(document).ready(function(){




        // var socket = io('http://127.0.0.1:3000');
        // // console.log(socket);
        // socket.on('laravel_database_barcode-channel:barcodeEvent', function(data){
        //     console.log(data);
        //     // showBarcodeData(data);
        // });

        // socket.on('laravel_database_weightlog-channel:weightLogEvent', function(data){
        //     console.log('Weightlog Data : '+data);
        //     // showBarcodeData(data);
        // });

        $('#udfw-doff-rw, #udfw-doff-val').on('input',function() {
          $(this).val($(this).val().toUpperCase());
        });



      $('.date').datetimepicker({
          format:'d-m-Y H:i',
      });

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

      $('input[type="text"]').attr('autocomplete', 'off');

      $('#rewinding-confirmation').on('click',function() {
        window.location.href = "/home";
      });

      $('#weight-log').on('click',function() {
        $('#doffModal').modal('show');
      });
      $('#udfw-weightlog').on('click',function() {
        $('#udfwDoffModal').modal('show');
      });
      $('#lf-rm').on('click',function(){
        window.location.href = "/spindle-update";
      });

      $('#weight-log-report').on('click',function() {
        $('#weightLogReport').modal('show');
      });

      $('#old-doff').on('change',function() {
        $('#udfw-doff-val').val($(this).val());
      })

      $('#doffModal').on('submit',function(e) {
        e.preventDefault();
        $pass = hashCode($('#doff-pass').val());
        if ($pass == '826631381') {
          window.location.href = "/weight-log/"+$('#doff').val();
        }else{
          alert('Invalid credentials..!');
        }
      });


      $('#udfwDoffModal').on('submit',function(e) {
        e.preventDefault();
        $pass = hashCode($('#udfw-doff-pass').val());
        if ($pass == '826631381') {
          window.location.href = "/udfw-weight-log/"+$('#udfw-doff-val').val();
        }else{
          alert('Invalid credentials..!');
        }
      });

      $('#send-erp').on('click',function() { 
        window.location.href = "/bulk-upload";
      })
      
      $('#udfw-send-erp').on('click',function() { 
        window.location.href = "/udfw-bulk-upload";
      })

      $('#reportSummary').on('click',function() {
        window.location.href = "/report-summary";
      })

      
    //   $('#send-erp').on('click',function() {
    //     window.location.href = "/bulk-upload";
    //   });
      
      function hashCode(str) {
        return str.split('').reduce((prevHash, currVal) =>
          (((prevHash << 5) - prevHash) + currVal.charCodeAt(0))|0, 0);
      }


      $('#udfw-rewinding').on('click',function() {
        $('#rewindingDoffModal').modal('show');
      });

      $('#rewindingDoffModal').on('submit',function(e) {
        e.preventDefault();
        $pass = hashCode($('#rewinding-pass').val());
        if ($pass == '826631381') {
          window.location.href = "/rewinding/"+$('#udfw-doff-rw').val();
        }else{
          alert('Invalid credentials..!');
        }
      });

      $('#udfw-ncr').on('click',function() {
        window.location.href = "/ncr-entry";
      });

    });
</script>
</body>
</html>
