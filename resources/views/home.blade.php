<html>
<head>
    <title>Inspection</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link rel="stylesheet" href="{{url('css/generate-indent.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />

    <style>
        #main{
            margin-top: 100px;
        }

        .wl-card{
            width: 100%;
            height: 400px;
            /*box-shadow: 0px 0px 5px rgba(0,0,0, 0.2);*/
            margin-top: 10%;
        }

        th{
            text-align: center;
        }

        tr>td:nth-child(3){
            width: 4%;
        }

        tr>td:nth-child(2){
            width: 10%;
        }

        tr>td:nth-child(4){
            width: 8%;
        }

        tr>td:nth-child(1){
            width: 10%;
        }

        tr>td{
            text-align: center;
        }

        .wl-card>h3{
            margin-top: 20px;
            margin-bottom: 20px;

        }

        select:disabled,input[type="text"]:disabled{
            background-color: gainsboro !important;
        }

        input[type='checkbox']{
            display: block;
        }


    </style>

    <div id="loader" class="loader"></div>
    <section id="header">
        <header>
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar-collapse" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                </div>
            </nav>
        </header>

    </section>
</head>
<body>

<section id="main">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="wl-card">
                    <h3>Rewinding Confirmation</h3>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Material</th>
                            <th>DOFF No</th>
                            <th>Spindle</th>
                            <th>Total Weight (kg)</th>
                            <th>Tare Weight(kg)</th>
                            <th>Material Weight(kg)</th>

                        </tr>
                        </thead>
                        <tbody>

                        <!--
                            <tr data-id="7398">
                                <td style="width: 300px;">2X3 SO 001</td>
                                <td style="width: 200px;">BPX 2/3</td>
                                <td>1</td>
                                <td>12.5</td>
                                <td>1</td>
                                <td>11.5</td>
                            </tr> -->
                        </tbody>
                    </table>
                    <button type="submit" class="btn btn-primary center-block" id="submit">Submit</button>
                </div>
            </div>
        </div>
    </div>



    <div id="responsePopup" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Rewinding Update Status</h4>
                </div>
                <div class="modal-body">
                    <h4 id="response"  style="margin-bottom: 20px;"></h4>
                    <input type="button" class="btn btn-primary center-block" style="width: 100px" onClick="window.location.reload()" value="Done">
                </div>
            </div>

        </div>
    </div>


    <?php //$reason = \App\NcrMaster::all(); ?>

</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
{{--<script src="{{url('js/simulate.js')}}"></script>--}}



<script>


    $(document).ready(function(){



        function  showBarcodeData(data){

            data = JSON.parse(data);
            wl = data.data;
            //console.log(data.status, data.data)
            if(data.status){
                $('tbody').empty();
                    $.each(data.data, function(key, wl){
                    console.log(key, wl);
                    if (data.data.length > 0) {
                        if(wl.active_status == 1){
                            $('tbody').append(
                                '<tr data-id="'+wl.id+'" data-unique_id="'+wl.unique_id+'" data-doffdate="'+wl.doff_date+'" data-doffno="'+wl.doff_no+'" data-materialid="'+wl.material_id+'" data-material="'+wl.material+'" data-floorcode="'+wl.floor_code+'">'+
                                '<td>'+wl.material+'</td>'+
                                '<td>'+wl.doff_no+'</td>'+
                                '<td>'+wl.spindle+'</td>'+
                                '<td>'+wl.total_weight+'</td>'+
                                '<td>'+wl.tare_weight+'</td>'+
                                '<td>'+wl.material_weight+'</td>'+
                                '</tr>'
                            );
                        }else if(wl.weight_status == 0){
                            alert('Cannot show Not ok spindle in Inspection');
                        }else{
                            alert('The Spindle is either packed or Inspected Already');
                        }
                    }else{
                        alert('Spindle Not In ERP.');
                    }
                    
                });
            }else{
                alert('No data found for this QR Code');
            }
        }


        var socket = io('http://127.0.0.1:3000');
        console.log(socket);
        socket.on('laravel_database_barcode-channel:barcodeEvent', function(data){
            console.log(data);
            showBarcodeData(data);
        });


        $(document).on('click', '#submit',function(){
            var tr = $(document).find('tbody > tr');

            // console.log(tr);
            var weightLogEntries = 'id='+tr.data('id')+'&doff_no='+tr.data('doffno')+'&doff_date='+tr.data('doffdate')+'&material='+tr.data('material')+'&floor_code='+tr.data('floorcode')+'&material_id='+tr.data('materialid')+'&unique_id='+tr.data('unique_id');

            console.log(weightLogEntries);

            // break;


            if(weightLogEntries.length > 0){
                $.ajax({
                    type: "POST",
                    url: "/rewind-in-process",
                    data: weightLogEntries,
                    beforeSend: function() {},
                    success: function(data, status, xhr) {
                        data = JSON.parse(data)
                        if(xhr.status == 200){
                            // alert('Updated Successfully');
                            if(data.status){
                                $('#response').text(data.response);
                                $('#responsePopup').modal('show');
                            }else{
                                $('#response').text(data.response);
                                $('#responsePopup').modal('show');
                            }

                        }
                    },
                    error: function(xhr, status, error) {
                        if (xhr.status == 422) {

                            alert(xhr.responseJSON.errors)

                        }
                    },
                })
            }
        });


        $.ajaxSetup({
            headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

    });


    $.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
    $.fn.modal.prototype.constructor.Constructor.DEFAULTS.keyboard =  false;

</script>

</body>
</html>
