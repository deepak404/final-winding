<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RewindingProcess extends Model
{
    protected $fillable = [
        'unique_id',
        'doff_no',
        'doff_date',
        'material_id',
        'material',
        'floor_code',
        'process_status',
        'erp_status'
    ];
}
