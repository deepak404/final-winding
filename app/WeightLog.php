<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeightLog extends Model
{
    protected $fillable = [
        'doff_no',
        'doff_date',
        'material_id',
        'material',
        'floor_code',
        'process_status',
        'spindle',
        'erp_status',
        'new_material',
        'tare_weight',
        'material_weight',
        'total_weight',
        'weight_status',
        'ncr_status',
        'reason',
        'wl_time',
        'op_name'
    ];
}
