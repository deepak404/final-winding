<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UdfwWeightLog extends Model
{
    protected $fillable = [

        'unique_id',
        'material_id',
        'material',
        'floor_code',
        'machine',
        'wl_time',
        'op_name',
        'doff_no',
        'spindle',
        'tare_weight',
        'material_weight',
        'total_weight',
        'weight_status',
        'rw_status',
        'doff_date',
        'reason',
        'filament_type',
        'ncr_status',
        'packing_name'

    ];
}
