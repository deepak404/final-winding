<?php

namespace App\Http\Controllers;

use App\UdfwBobbin;
use App\UdfwFilament;
use App\UdfwItemMaster;
use App\UdfwNcrMaster;
use App\UdfwWeightLog;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UndippedController extends Controller
{
    public function UdfwWeightLog($doff)
    {
        $doff = str_replace(' ', '', $doff);
        $status = self::doffCheck($doff);
        if (!$status) {
            return redirect('/')->withErrors(['error' => $doff.' - Invalid format Doff No.']);
        }
        if (date('m') <= 3) {
            $financial_start = '01-04-'.(date('Y')-1);
            $financial_end = date('31-03-Y');
        } else {
            $financial_start = date('01-04-Y');
            $financial_end = '31-03-'.(date('Y') + 1);
        }
        $financial_start = date('Y-m-d 00:00:00', strtotime($financial_start));
        $financial_end = date('Y-m-d 23:59:59', strtotime($financial_end));

        $lastDoff = UdfwWeightLog::latest()->first();
        $financial_start = date('Y-m-d 00:00:00', strtotime($financial_start));
        $financial_end = date('Y-m-d 23:59:59', strtotime($financial_end));
        $weightLog = UdfwWeightLog::where('doff_no', $doff)
                            ->where('doff_date', '>=', $financial_start)
                            ->where('doff_date', '<=', $financial_end)
                            ->get()
                            ->sortBy('spindle');
        $material = null;
        $operator = "";
        $fromDate = $this->getFromDate();
        if (count($weightLog) > 0) {
            $doffDate = date('d-m-Y H:i', strtotime($weightLog->first()->doff_date));
            $material = ['id'=>$weightLog->first()->material_id,'material'=>$weightLog->first()->material];
            $operator = $weightLog->first()->op_name;
        } else {
            $doffDate = date('d-m-Y H:i');
        }
        
        $itemMaster = UdfwItemMaster::all();
        $count = $weightLog->count();
        $split = (int)round($count/2);
        $bobbins = UdfwBobbin::all();
        $reason = UdfwNcrMaster::all();
        $last = $weightLog->last();
        $filament = UdfwFilament::all();
        return view('udfw.weightlog')->with(compact('split', 'itemMaster', 'bobbins', 'weightLog', 'doff', 'doffDate', 'material', 'operator', 'reason', 'last', 'filament', 'fromDate', 'lastDoff'));
    }
    
    /**
     * doffCheck function
     *
     * @param [string] $doff
     * @return bool
     */
    public function doffCheck($doff)
    {
        $doffSplit = explode('/', $doff);
        $numeric = false;

        if (count($doffSplit) > 1) {
            if (!is_numeric($doffSplit[1])) {
                return false;
            }
        } else {
            for ($i=0; $i < strlen($doff) ; $i++) {
                if (!$numeric) {
                    if (is_numeric($doff[$i])) {
                        $numeric = true;
                    }
                } elseif (!is_numeric($doff[$i])) {
                    return false;
                }
            }
            if (!$numeric) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get Shift Time function
     *
     * @return time
     */
    public function getFromDate()
    {
        $nowDate = date('H:i');
        if (date('H:i', strtotime('07:00')) <= $nowDate && $nowDate <= date('H:i', strtotime('15:00'))) {
            $shift = date('d-m-Y 07:00');
        } elseif (date('H:i', strtotime('15:01')) <= $nowDate && $nowDate <= date('H:i', strtotime('23:00'))) {
            $shift = date('d-m-Y 15:01');
        } else {
            $shift = date('d-m-Y 00:01');
        }

        return $shift;
    }

    public function UdfwWeightLogCreate(Request $request)
    {
        $processData = $request->all();
        $rwStatus = $request['rw_status'] ?? 0;
        try {
            if ($processData['weight_status'] == 1) {
                $weightStatus = 1;
            } else {
                $weightStatus = 0;
            }
            $thisDate = Carbon::now()->timezone('Asia/Kolkata')->format('Y-m-d H:i:s');

            $material= UdfwItemMaster::where('id', $processData['material'])->first();

            if (!empty($processData['unique_id'])) {
                $unique_id = $processData['unique_id'];
            } else {
                $unique_id = 'udfw'.time();
            }

            $weightLog = UdfwWeightLog::create([
                    'unique_id' => $unique_id,
                    'material_id' => $processData['material'],
                    'material' => $material->material,
                    'packing_name' => $material->packing_name,
                    'floor_code' => $material->descriptive_name,
                    'machine' => 'UDFW',
                    'wl_time' => $thisDate,
                    'op_name' => $processData['operator'],
                    'doff_no' => $processData['doff_no'],
                    'spindle' => $processData['spindle_no'],
                    'tare_weight' => $processData['tare_weight'],
                    'material_weight' => round($processData['actual_weight'], 2),
                    'total_weight' => $processData['scale_weight'],
                    'weight_status' => $weightStatus,
                    'rw_status' => $rwStatus,
                    'reason' => $processData['reason'] ?? null,
                    'ncr_status' => 0,
                    'filament_type'=>$processData['filament'],
                    'doff_date' => date('Y-m-d H:i:s', strtotime($processData['doff_time'])),
                    'erp_status' => 0
                    ]);

            $this->printQrCode($weightLog);
            $this->printQrCode($weightLog);
            
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "erp.shakticords.com/api/weight-log");
            curl_setopt($ch, CURLOPT_POST, 1);

            curl_setopt(
                $ch,
                CURLOPT_POSTFIELDS,
                http_build_query(
                    array(
                      "api-key"=> "UNICO2019wl",
                      "UniqID"=> $weightLog->unique_id,
                      "wl_eq_id"=> 'UDFW',
                      "wl_date"=> $thisDate,
                      "wl_op"=> $processData['operator'],
                      "wl_part"=> $material->material,
                      "wl_filament"=>$processData['filament'],
                      "wl_floor_code" => $material->descriptive_name,
                      "wl_packing_name" => $material->packing_name,
                      "wl_doff"=> $processData['doff_no'],
                      "wl_spindle"=> $processData['spindle_no'],
                      "wl_tare"=> $processData['tare_weight'],
                      "wl_weight"=> round($processData['actual_weight'], 2),
                      "wl_scale_wt"=> $processData['scale_weight'],
                      "wl_ok"=> $weightStatus,
                      "wl_rw"=> $rwStatus,
                      "wl_min"=> 0,
                      "wl_max"=> 100,
                      "wl_dt_doff"=> $processData['doff_time'],
                      "reason"=> $processData['reason'] ?? null,
                      "update"=>false,
                      "ncr_status"=>0,
                      "inspection_reason"=>null,
                      "inspection_status"=>0,
                      "active_status"=>1,
                      "wl_id"=>$weightLog->id,
                      "inspection_status"=>$weightStatus
                        )
                )
            );

            // receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = json_decode(curl_exec($ch));

            curl_close($ch);

            if ($result->status == 1) {
                UdfwWeightLog::where('id', $weightLog->id)->update(['erp_status'=>1]);

                return response()->json(['status'=>1,'msg'=>'Successfully Created.']);
            } else {
                UdfwWeightLog::where('id', $weightLog->id)->update(['erp_status'=>0]);

                return response()->json(['status'=>0,'msg'=>$result->msg]);
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>0,'msg'=> $e->getMessage()]);
        }
    }

    public function printQrCode(UdfwWeightLog $weightLog)
    {
        // $descriptiveName = UdfwItemMaster::where('material', $weightLog->material)->value('descriptive_name');
        $dop = date('d-m-Y', strtotime($weightLog->doff_date));

        $text = "'Seagull:2.1:DP
                INPUT OFF
                VERBOFF
                INPUT ON
                SYSVAR(48) = 0
                ERROR 15,\"FONT NOT FOUND\"
                ERROR 18,\"DISK FULL\"
                ERROR 26,\"PARAMETER TOO LARGE\"
                ERROR 27,\"PARAMETER TOO SMALL\"
                ERROR 37,\"CUTTER DEVICE NOT FOUND\"
                ERROR 1003,\"FIELD OUT OF LABEL\"
                SYSVAR(35)=0
                OPEN \"tmp:setup.sys\" FOR OUTPUT AS #1
                PRINT#1,\"Printing,Media,Print Area,Media Margin (X),0\"
                PRINT#1,\"Printing,Media,Clip Default,On\"
                CLOSE #1
                SETUP \"tmp:setup.sys\"
                KILL \"tmp:setup.sys\"
                CLL
                OPTIMIZE \"BATCH\" ON
                PP25,183:AN7
                BARSET \"QRCODE\",1,1,5,2,1
                PB \"$weightLog->unique_id\"
                PP155,188:NASC 8
                FT \"Univers Bold\"
                FONTSIZE 8
                FONTSLANT 0
                PT \"$weightLog->material\"
                PP155,158:PT \"DOFF : $weightLog->doff_no\"
                PP155,129:PT \"DOP : $dop\"
                PP155,98:PT \"SPINDLE NO : $weightLog->spindle\"
                PP21,64:FONTSIZE 5
                PT \"$weightLog->floor_code\"
                LAYOUT RUN \"\"
                PF
                PRINT KEY OFF";



        file_put_contents('output.prn', $text);

        shell_exec('COPY ' .public_path(). '\output.prn /B \\\127.0.0.1\honeywell');
    }

    public function UdfwRewinding($doff = null)
    {
        $doff = str_replace(' ', '', $doff);
        $status = self::doffCheck($doff);
        if (!$status) {
            return redirect('/')->withErrors(['error' => 'Invalid format Doff No.']);
        }
        if (date('m') <= 3) {
            $financial_start = '01-04-'.(date('Y')-1);
            $financial_end = date('31-03-Y');
        } else {
            $financial_start = date('01-04-Y');
            $financial_end = '31-03-'.(date('Y') + 1);
        }
        $lastDoff = UdfwWeightLog::latest()->first();
        $financial_start = date('Y-m-d 00:00:00', strtotime($financial_start));
        $financial_end = date('Y-m-d 23:59:59', strtotime($financial_end));
        $weightLog = UdfwWeightLog::where('doff_no', $doff)
                            ->where('doff_date', '>=', $financial_start)
                            ->where('doff_date', '<=', $financial_end)
                            ->get()
                            ->sortBy('spindle');
        $material = null;
        $operator = "";
        $fromDate = $this->getFromDate();
        if (count($weightLog) > 0) {
            $doffDate = date('d-m-Y H:i', strtotime($weightLog->first()->doff_date));
            $material = ['id'=>$weightLog->first()->material_id,'material'=>$weightLog->first()->material];
            $operator = $weightLog->first()->op_name;
        } else {
            $doffDate = null;
        }
        $itemMaster = UdfwItemMaster::all();
        $bobbins = UdfwBobbin::all();
        $reason = UdfwNcrMaster::all();
        $last = $weightLog->last();
        $filament = UdfwFilament::all();
        $count = $weightLog->count();
        $split = (int)round($count/2);

        return view('udfw.rewinding')
                ->with(
                    compact(
                        'split',
                        'itemMaster',
                        'bobbins',
                        'weightLog',
                        'doff',
                        'doffDate',
                        'material',
                        'operator',
                        'reason',
                        'last',
                        'filament',
                        'fromDate',
                        'lastDoff'
                    )
                );
    }

    public function ncrEntry()
    {
        $bobbins = UdfwBobbin::all();
        $reason = UdfwNcrMaster::all();
        $ncrLog = UdfwWeightLog::where('weight_status', 0)
                          ->where('rw_status', '1')
                          ->where('ncr_status', '1')
                          ->orderBy('id', 'desc')
                          ->get();
        return view('udfw.ncr-packing')->with(compact('bobbins', 'reason', 'ncrLog'));
    }

    public function GetPrintQr(Request $request)
    {
        $weightLog = UdfwWeightLog::where('id', $request['id'])->first();
        $this->printQrCode($weightLog);
    }

    public function deleteWeightLog(Request $request)
    {
        if ($request['password'] == 'madurai') {
            $weightLog = UdfwWeightLog::where('id', $request['id'])->first();
            if ($weightLog->erp_status == 1) {
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, "erp.shakticords.com/api/delete-weight-log");
                curl_setopt($ch, CURLOPT_POST, 1);
    
                curl_setopt(
                    $ch,
                    CURLOPT_POSTFIELDS,
                    http_build_query(
                        array(
                            "api-key"=> "UNICO2019wl",
                            'wl_id'=>$weightLog->id,
                            'machine'=>$weightLog->machine,
                            )
                    )
                );
    
                // receive server response ...
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
                $result = json_decode(curl_exec($ch));
    
                curl_close($ch);

                if (empty($result->status)) {
                    return response()->json(['status'=>0,'msg'=>'Unable to connect to ERP.']);
                }

                if ($result->status == true) {
                    $weightLog->delete();
                    return response()->json(['status'=>1,'msg'=>'Successfully deleted.']);
                } else {
                    return response()->json(['status'=>0,'msg'=>$result->msg]);
                }
            } else {
                $weightLog->delete();
                return response()->json(['status'=>1,'msg'=>'Successfully deleted.']);
            }
            
            // Weightlog::where('id', $request['id'])->delete();
        }
        return response()->json(['status'=>0,'msg'=>'Invalid Password..!']);
    }

    public function getQr(Request $request)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "erp.shakticords.com/api/get-barcode-rewinding");
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            http_build_query(
                array(
                    'unique_id'=>$request['unique_id']
                      )
            )
        );

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = json_decode(curl_exec($ch));

        curl_close($ch);
        if ($result->status == false) {
            return response()->json(['status'=>false,'msg'=>'No data found try other QR Code.']);
        }
        // http_response_code(500);
        // dd($result->data);
        // return $result->data;

        $weightStatus = 1;
        foreach ($result->data as $key => $value) {
            if ($value->weight_status == 0) {
                $weightStatus = 0;
            }
        }

        if ($weightStatus == 1) {
            return response()->json(['status'=>false,'msg'=>'Scanned QR weight log status Ok. Try Other Spindle.']);
        } else {
            return response()->json(['status'=>true,'entry'=>$result->data]);
        }
    }


    public function udfwBulkUpload()
    {
        $pendingWeight = UdfwWeightLog::where('erp_status', 0)->get()->toArray();
        // dd(json_encode($pendingWeight));

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "erp.shakticords.com/api/bulk-upload");
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            http_build_query(
                array(
                    'entry'=>json_encode($pendingWeight),
                      )
            )
        );

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);

        if (is_null($result)) {
            return redirect('/');
        }
        curl_close($ch);
        $result = json_decode($result);
        if ($result->status == true) {
            UdfwWeightLog::where('erp_status', 0)->update(['erp_status'=>1]);
            return redirect('/');
        } else {
            return redirect('/');
        }
    }

    public function qrNcr(Request $request)
    {
        $data = file('http://erp.shakticords.com/api/get-qr-ncr/'.$request['uniqueId']);
        $data = json_decode($data[0]);

        if ($data->status == false) {
            return response()->json(['status'=>$data->status,'msg'=>$data->msg]);
        }
        return response()->json(['status'=>$data->status,'entry'=>$data->data]);
    }

    public function udfwCreateNcr(Request $request)
    {
        $oldEntry = json_decode($request['old_entry']);

        $material= UdfwItemMaster::where('material', $oldEntry->material)->first();

        $thisDate = Carbon::now()->timezone('Asia/Kolkata')->format('Y-m-d H:i:s');

        $weightLog = UdfwWeightLog::create([
                    'unique_id' => $oldEntry->unique_id,
                    'material_id' => $material->id,
                    'material' => $material->material,
                    'floor_code' => $material->descriptive_name,
                    'packing_name' => $material->packing_name,
                    'machine' => "UdFW",
                    'wl_time' => $thisDate,
                    'op_name' => $oldEntry->op_name,
                    'doff_no' => $oldEntry->doff_no,
                    'spindle' => $request['spindle_no'],
                    'tare_weight' => $request['tare_weight'],
                    'material_weight' => $request['scale_weight'],
                    'total_weight' => $request['actual_weight'],
                    'weight_status' => 0,
                    'rw_status' => 1,
                    'reason' => $request['reason'],
                    'ncr_status' => 1,
                    'filament_type'=>$oldEntry->filament,
                    'doff_date' => date('Y-m-d H:i:s', strtotime($oldEntry->doff_date)),
                    'erp_status' => 0
                  ]);

        $this->printQrCode($weightLog);

        $this->printQrCode($weightLog);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "erp.shakticords.com/api/create-ncr");
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            http_build_query(
                array(
                    'id'=>$oldEntry->id,
                    'spindle'=>$request['spindle_no'],
                    'tare_weight'=>$request['tare_weight'],
                    'reason'=>$request['reason'],
                    'scale_weight'=>$request['scale_weight'],
                    'actual_weight'=>$request['actual_weight'],
                    'wl_time'=>$weightLog->wl_time,
                    "wl_id"=>$weightLog->id
                      )
            )
        );

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = json_decode(curl_exec($ch));

        curl_close($ch);

        if ($result->status == 1) {
            $update = UdfwWeightLog::where('id', $weightLog->id)->update(['erp_status'=>1]);

            return response()->json(['status'=>1,'msg'=>'Successfully Created.']);
        } else {
            return response()->json(['status'=>0,'msg'=>$result->msg]);
        }
    }

    public function reportSummary()
    {
        $fromDate = $this->getFromDate();

        return view('udfw.report-summary')->with(compact('fromDate'));
    }

    public function getSummary(Request $request)
    {
        $from = $request['from'];
        $to = $request['to'];

        if ($request['type'] == 'weight') {
            $weightLog = UdfwWeightLog::where('wl_time', '>=', date('Y-m-d H:i', strtotime($from)))
                              ->where('wl_time', '<=', date('Y-m-d H:i', strtotime($to)))
                              ->get()
                              ->groupBy('doff_no');
            $summary = [];
            $count = 0;
            foreach ($weightLog as $key => $value) {
                $summary[$count]['doff'] = $key;
                $summary[$count]['count'] = $value->first()->material;
                $count++;
            }

            return response()->json(['status'=>1,'logs'=>$summary]);
        } else {
            $weightLog = UdfwWeightLog::where('doff_date', '>=', date('Y-m-d H:i', strtotime($from)))
                              ->where('doff_date', '<=', date('Y-m-d H:i', strtotime($to)))
                              ->get()
                              ->groupBy('doff_no');
            $summary = [];
            $count = 0;
            foreach ($weightLog as $key => $value) {
                $summary[$count]['doff'] = $key;
                $summary[$count]['count'] = $value->first()->material;
                $count++;
            }

            return response()->json(['status'=>1,'logs'=>$summary]);
        }
    }
}
