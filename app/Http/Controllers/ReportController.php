<?php

namespace App\Http\Controllers;

use App\UdfwWeightLog;
use Illuminate\Http\Request;
use PDF;
use App\WeightLog;

class ReportController extends Controller
{
    
    public function weightLogReport(Request $request)
    {
        $from = $request['from'];
        $to = $request['to'];
        
        $weightLog = WeightLog::where('wl_time', '>=', date('Y-m-d H:i:00', strtotime($from)))
                            ->where('wl_time', '<=', date('Y-m-d H:i:59', strtotime($to)))
                            ->get()
                            ->groupBy('doff_no');
        return PDF::loadView('reports.weight-log-report',[
                                    'weightlog'=>$weightLog,
                                    'from'=>$from,
                                    'to'=>$to
                            ])
                    ->setPaper('a4', 'portrait')
                    ->setWarnings(false)
                    ->stream('Weight Log Report.pdf');
    }

    public function weightLogSummary(Request $request)
    {
        $from = $request['from'];
        $to = $request['to'];

        if ($request['report_type'] == 'weight') {
            $weightLog = UdfwWeightLog::where('wl_time', '>=', date('Y-m-d H:i:00', strtotime($from)))
                              ->where('wl_time', '<=', date('Y-m-d H:i:59', strtotime($to)))
                              ->where('rw_status', 0)
                              ->get()
                              ->groupBy('doff_no');
        } else {
            $weightLog = UdfwWeightLog::where('doff_date', '>=', date('Y-m-d H:i:00', strtotime($from)))
                              ->where('doff_date', '<=', date('Y-m-d H:i:59', strtotime($to)))
                              ->where('rw_status', 0)
                              ->get()
                              ->groupBy('doff_no');
        }
        return PDF::loadView('reports.weight-log-summary', ['weightLog'=>$weightLog,'from'=>$from,'to'=>$to])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Weight Log Summary.pdf');

    }
}
