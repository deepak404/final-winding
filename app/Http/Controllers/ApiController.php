<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;
use App\RewindingProcess;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getBarcodeDetails(Request $request)
    {
        $apiKey = $request['api-key'];



        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('POST', 'http://erp.shakticords.com/api/get-barcode-weight-log', [
            'form_params' => [
                'unique_id' => $request['data'],
                'api_key' => 'scpl-inspection-003',
            ]
        ]);

        $data = [
            'event' => $request['event'],
            'data' =>  $response->getBody()->getContents(),
        ];


        Redis::publish('barcode-channel', json_encode($data));
    }



    public function rewindInProcess(Request $request)
    {
        $request->validate([
            'id'=>'required',
            'doff_no'=>'required',
            'doff_date'=>'required',
            'material'=>'required',
            'floor_code'=>'required',
            'material_id'=>'required',
            'unique_id'=>'required'
        ]);

        $rewindingProcess = RewindingProcess::where('doff_no', $request['doff_no'])->where('doff_date', $request['doff_date'])->first();

        if (!is_null($rewindingProcess)) {
            $uniqueId = $rewindingProcess->unique_id.'|'.$request['unique_id'];
            RewindingProcess::where('doff_no', $request['doff_no'])
                            ->where('doff_date', $request['doff_date'])
                            ->update(['unique_id'=>$uniqueId]);
        } else {
            RewindingProcess::create($request->all());
        }

        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('POST', 'http://erp.shakticords.com/api/update-rewinding-inprocess', [
            'form_params' => [
                'weight_log_entries' => $request['id'],
                'api_key' => 'scpl-inspection-003',
            ]
        ]);
        
        $responseBody = $response->getBody()->getContents();
        $responseBody = json_decode($responseBody);

        if ($responseBody->status ?? false) {
            RewindingProcess::where('doff_no', $request['doff_no'])
                            ->where('doff_date', $request['doff_date'])
                            ->update(['erp_status'=>1]);
        }

        return json_encode($responseBody);


    }


    public function sendWeightLog(Request $request){


        $data = [
            'event' => $request['event'],
            'data' => $request['data']
        ];

        Redis::publish('weightlog-channel', json_encode($data));
        
    }
}
