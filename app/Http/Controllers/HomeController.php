<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RewindingProcess;
use App\WeightLog;
use App\ItemMaster;
use App\NcrMaster;
use App\Bobbin;
use App\UdfwWeightLog;
use GuzzleHttp\Client;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function welcome()
    {
        $rewindingProcess = RewindingProcess::where('process_status', 1)->get();
        $count = 0;
        $udfwCount = 0;
        $count += RewindingProcess::where('erp_status', 0)->count();
        $count += WeightLog::where('erp_status', 0)->count();
        $udfwCount += UdfwWeightLog::where('erp_status', 0)->count();
        $doffs = UdfwWeightLog::distinct()->pluck('doff_no')->take(-10)->toArray();
        $fromDate = $this->getFromDate();
        return view('welcome')->with(compact('rewindingProcess', 'count', 'fromDate', 'doffs', 'udfwCount'));
    }

    public function weightLogCreate($doff = null)
    {
        $rewindingProcess = RewindingProcess::where('id', $doff)->where('process_status', 1)->first();
        $doffs = RewindingProcess::where('process_status', 1)->get();
        if (is_null($rewindingProcess)) {
            return redirect('/')->withErrors(['error' => 'Invalid Doff No.']);
        }
        $weightLog = WeightLog::where('doff_no', $rewindingProcess->doff_no)
                                ->where('doff_date', $rewindingProcess->doff_date)
                                ->orderBy('spindle')
                                ->get();
        $count = $weightLog->count();
        $split = (int)round($count/2);
        $itemMaster = ItemMaster::where('material', $rewindingProcess->material)->get();
        $reason = NcrMaster::all();
        $bobbins = Bobbin::all();
        $total = ['ok'=>$weightLog->where('weight_status', 1)->sum('material_weight'),
                  'not_ok'=>$weightLog->where('weight_status', 0)->sum('material_weight'),
                  'wt'=>$weightLog->sum('material_weight'),
                  'entry'=>$count
                ];
        // dd($split);
        return view('weight-log')->with(compact('rewindingProcess', 'weightLog', 'doffs', 'doff', 'itemMaster', 'reason', 'bobbins', 'total', 'split'));
    }

    public function CreateWeightLog(Request $request)
    {
        $processed = $request->all();
        $uri = '/weight-log/'.$request['doff'];
        $checkSpindle = WeightLog::where('doff_no', $processed['doff_no'])->where('spindle', $processed['spindle'])->get()->count();
        if ($checkSpindle > 0) {
            return redirect($uri)->withErrors(['error' => 'Spindle already exists...']);
        }
        try {
            if ($processed['material_weight'] >= $processed['total_weight'] || $processed['material_weight'] < 0 ||  $processed['total_weight'] < 0) {
                return redirect($uri)->withErrors(['error' => 'Actual Weight should be lower then scale weight.']);
            }
            unset($processed['_token']);
            unset($processed['doff']);
            $processed['wl_time'] = date('Y-m-d H:i:s');
            $weightLog = WeightLog::create($processed);
            $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
            $response = $client->request('POST', 'http://erp.shakticords.com/api/create-rewind-weightlog', [
                'form_params' =>[
                    'doff_no' => $weightLog['doff_no'],
                    'doff_date' => $weightLog['doff_date'],
                    'material' => $weightLog['material'],
                    'material_weight' => $weightLog['material_weight'],
                    'material_id' => $weightLog['material_id'],
                    'new_material' => $weightLog['new_material'],
                    'total_weight' => $weightLog['total_weight'],
                    'ncr_status' => $weightLog['ncr_status'],
                    'reason' => $weightLog['reason'],
                    'spindle' => $weightLog['spindle'],
                    'tare_weight' => $weightLog['tare_weight'],
                    'floor_code' => $weightLog['floor_code'],
                    'weight_status' => $weightLog['weight_status'],
                    'wl_time' => $weightLog['wl_time'],
                    'wl_id' => $weightLog['id'],
                    'op_name' => $weightLog['op_name'],


                ]
            ]);
        
            $responseBody = $response->getBody()->getContents();
            $responseBody = json_decode($responseBody);

            if ($responseBody->status ?? false) {
                WeightLog::where('id', $weightLog->id)
                                ->update(['erp_status'=>1]);
            }
            return redirect($uri);
        } catch (\Exception $th) {
            return redirect($uri)->withErrors(['error' => $th->getMessage()]);
        }
    }

    public function deleteWeightLog(Request $request)
    {
        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('POST', 'http://erp.shakticords.com/api/delete-rewind-weightlog', [
            'form_params' =>[
                'id' => $request['wl_id'],
            ]
        ]);

        $responseBody = $response->getBody()->getContents();
        $responseBody = json_decode($responseBody);
        if ($responseBody->status ?? false) {
            WeightLog::where('id', $request['wl_id'])
                            ->delete();
            return response()->json(['status'=>1]);
        }
        return response()->json(['status'=>0,'msg'=>$responseBody->response ?? 'Unable to delete.']);
    }

    public function bulkUpload()
    {
        try {
            $rewindingProcess = RewindingProcess::where('erp_status', 0)->pluck('unique_id');
            $uniqueId = [];
            foreach ($rewindingProcess as $value) {
                foreach (explode('|', $value) as $data) {
                    $uniqueId[] = $data;
                }
            }
            $weightLog = WeightLog::where('erp_status', 0)->get()->toArray();
            if (count($uniqueId) > 0 || count($weightLog) > 0) {
                $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
                $response = $client->request('POST', 'http://erp.shakticords.com/api/bulk-kidde-upload', [
                    'form_params' =>[
                        'weight_logs' => json_encode($weightLog),
                        'rewinding' => $uniqueId,
                    ]
                ]);
    
                $responseBody = $response->getBody()->getContents();
                $responseBody = json_decode($responseBody);
    
                if ($responseBody->status ?? false) {
                    RewindingProcess::where('erp_status', 0)->update(['erp_status'=>1]);
                    WeightLog::where('erp_status', 0)->update(['erp_status'=>1]);
                    return redirect('/');
                }
                if ($responseBody->data ?? false) {
                    WeightLog::where('erp_status', 0)->update(['erp_status'=>1]);
                    return redirect('/')->withErrors(['error'=>$responseBody->response ?? 'Unable to update.']);
                }
                return redirect('/')->withErrors(['error'=>$responseBody->response ?? 'Unable to update.']);
            } else {
                return redirect('/')->withErrors(['error'=>'No Data to Upload.']);
            }
        } catch (\Exception $th) {
            return redirect('/')->withErrors(['error'=>$th->getMessage()]);
        }
    }

    public function getFromDate()
    {
        $nowDate = date('H:i');
        if (date('H:i', strtotime('07:00')) <= $nowDate && $nowDate <= date('H:i', strtotime('15:00'))) {
            $shift = date('d-m-Y 07:00');
        } elseif (date('H:i', strtotime('15:01')) <= $nowDate && $nowDate <= date('H:i', strtotime('23:00'))) {
            $shift = date('d-m-Y 15:01');
        } else {
            $shift = date('d-m-Y 00:01');
        }

        return $shift;
    }

    public function spindleUpdate()
    {
        return view('spindle-update');
    }

    public function spindleUpdateChange(Request $request)
    {
        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);

        $response = $client->request('POST', 'http://erp.shakticords.com/api/update-kidde-spindle', [
            'form_params' => [
                'id' => $request['id'],
                'unique_id' => $request['unique_id'],
                'scale' => $request['scale'],
                'status' => $request['status'],
                'api_key' => 'scpl-finalwinding-001',
            ]
        ]);
        
        $responseBody = json_decode($response->getBody()->getContents());

        if ($responseBody->status == false) {
            return response()->json(['status'=>false,'response'=>$responseBody->response ?? 'Unable to update.']);
            ;
        } elseif ($responseBody->status) {
            return response()->json(['status'=>true]);
        }
    }
}
