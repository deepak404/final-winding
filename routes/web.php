<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome');

// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/rewind-in-process', 'ApiController@rewindInProcess');

Route::get('/weight-log/{doff}', 'HomeController@weightLogCreate');

Route::post('/create-weight-log', 'HomeController@CreateWeightLog');

Route::post('/delete-spindle', 'HomeController@deleteWeightLog');

Route::get('/bulk-upload', 'HomeController@bulkUpload');

Route::post('/weight-log-report', 'ReportController@weightLogReport');

Route::get('/spindle-update', 'HomeController@spindleUpdate');

Route::post('/spindle-update-change', 'HomeController@spindleUpdateChange');

// ---------------------- Un-dipped WeightLog ------------------------------------

Route::get('/udfw-weight-log/{doff}', 'UndippedController@UdfwWeightLog')->where('doff', '(.*)');

Route::get('/rewinding/{doff}', 'UndippedController@UdfwRewinding')->where('doff', '(.*)');

Route::post('/udfw-weight-create', 'UndippedController@UdfwWeightLogCreate');

Route::get('/ncr-entry', 'UndippedController@ncrEntry');

Route::post('/delete-weight-log', 'UndippedController@deleteWeightLog');

Route::post('/print-qr', 'UndippedController@GetPrintQr');

Route::post('/getQR', 'UndippedController@getQr');

Route::get('/udfw-bulk-upload', 'UndippedController@udfwBulkUpload');

Route::post('/get-qr-ncr', 'UndippedController@qrNcr');

Route::post('/udfw-create-ncr', 'UndippedController@udfwCreateNcr');

Route::get('/report-summary', 'UndippedController@reportSummary');

Route::post('/get-summary', 'UndippedController@getSummary');

Route::post('/generate-report', 'ReportController@weightLogSummary');

